#!/usr/bin/perl

use strict;
use warnings;
use LWP::UserAgent;
use JSON;
use Switch;

# change these as needed
my $clientID = "xxxxx"; # https://dev.twitch.tv/docs/authentication/register-app/
my $user = "xxxxx"; # numerical ID of moderator
my $channel = "xxxxx"; # numerical ID of channel to ban from
my $oauthToken = "xxxxx"; # moderator:manage:banned_users scope
my $banReason = "Known bot";

my $ua = LWP::UserAgent->new();

# fetch numerical user ids
# TODO: maybe use hash keys instead of two arrays
sub get_user_info {
    my @ref = @_;
    my $getURL = "https://api.twitch.tv/helix/users?login=" . lc($ref[0]);
    shift @ref;
    foreach (@ref) {
       $getURL = $getURL . "&login=" . lc($_);
    }
    my $res = $ua->get(
        $getURL,
        "Authorization" => "Bearer " . $oauthToken,
        "Client-Id" => $clientID
    );
    my $decodedInfo = decode_json($res->content);
    my @ids = ();
    my @names = ();
    for my $i (0 .. $#_) {
        push (@ids, $decodedInfo->{'data'}[$i]{'id'});
        push (@names, $decodedInfo->{'data'}[$i]{'login'});
    }
    @ids = grep defined, @ids;
    @names = grep defined, @names;
    return \@ids, \@names;
}

sub ban_user_by_id {
    my $bannedUserID = $_[0];
    if ($bannedUserID) {
        sleep(0.256);
        my $banURL = "https://api.twitch.tv/helix/moderation/bans?broadcaster_id=" .
            $channel . "&moderator_id=" . $user;
        my $banJSON = '{"data": {"user_id":"' . $bannedUserID .
            '","reason":"' . $banReason . '"}}';
        my $req = HTTP::Request->new('POST', $banURL);
        $req->header(
            "Authorization" => "Bearer " . $oauthToken,
            "Client-Id" => $clientID,
            "Content-Type" => "application/json"
        );
        $req->content($banJSON);
        my $res =  $ua->request($req);
        # successful calls don't return a useful array
        # checking HTTP status manually instead
        my $success = $res->is_success();
        $res = decode_json($res->content);
        if ($success) {
            return 200;
        } elsif ($res->{'status'} == 429) {
            print "rate limit exceeded.\n" . 
            "Waiting 1m and trying again.\n";
            sleep(60);
            ban_user_by_id($bannedUserID);
        } else {
            return $res->{status};
        }
    } else {
        return 0;
    }
}

# kind of a misnomer
# this routine exists for error handling
sub ban_users {
    my @names = @{$_[1]};
    my @ids = @{$_[0]};
    while (my ($i, $bannedID) = each @ids) {
        if ($bannedID) {
            print "Banning " . $names[$i] . ": ";
            switch(ban_user_by_id($bannedID)) {
                case 200 {print "success.\n"}
                case 400 {print "bad request.  User may already be banned.\n"}
                case 401 {print "unauthorized.\n"}
                case 403 {print "forbidden.\n"}
                case 409 {print "conflict.\n"}
                else {print "error.\n"}
            }
        } else {
            print "Flagrant error.\n";
        }
    }
}

# handle users that don't exist
sub find_exceptions {
    my @banlist = @{$_[0]};
    my @resultList = @{$_[1]};
    foreach my $userName (@banlist) {
        my $matches = grep $_ eq $userName, @resultList;
        if ($matches == 0) {
            print "User " . $userName . " does not seem to exist.\n";
        }
    }
}

my $banlist = "banlist.txt"; 
open(FH, '<', $banlist) or die $!;
my @currentNames = ();
my $nameCounter = 0;

# run queries every 100 names
# kludge to get around rate limiting
while (<FH>) {
    chomp;
    $nameCounter++;
    push(@currentNames, $_);
    if ($nameCounter == 100) {
        my ($ids, $names) = get_user_info(@currentNames);
        my @ids = @{$ids};
        my @names = @{$names};
        find_exceptions(\@currentNames, \@names);
        ban_users(\@ids, \@names);
        $nameCounter = 0;
        @currentNames = ();
    }
}

close (FH);

# run any remainder
# or all of them if fewer than 100 to begin with
if (@currentNames) {
    my ($ids, $names) = get_user_info(@currentNames);
    my @ids = @{$ids};
    my @names = @{$names};
    find_exceptions(\@currentNames, \@names);
    ban_users(\@ids, \@names);
}

