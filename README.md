# Auto Twitch Ban

A script for banning a large number of bot accounts on Twitch, written in Perl and using Twitch's Helix API.

Written to scratch my own itch, as existing scripts used IRC, which no longer accepts chat commands.  It's a pile of spaghetti that requires too much setup, but it works for me.  Do with it what you will.  Point and laugh perhaps.

# Perl module dependencies:
LWP::Protocol::https

JSON

Switch

# General requirements:
Numerical user IDs for both the moderator and channel

Client ID from your own app

OAuth token

A plaintext file called banlist.txt in the same directory as the script, with one banned user per line
